package com.muhardin.endy.training.microservices.aplikasipembayaran.dao;

import com.muhardin.endy.training.microservices.aplikasipembayaran.entity.VirtualAccount;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface VirtualAccountDao extends PagingAndSortingRepository<VirtualAccount, String> {
}
